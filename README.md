# Groupe 2 QSI

## Factory DAO

Membre du groupe:

- Mouctar FOFANA
- Arnaud KADERI
- Mame Aby Magathe NIANG
- Mamy Koloina RAKOTONDRAMANANA

Pour compiler le projet: `make compile`

Pour le déployer: `make deploy`

Lien de déploiement du smart contract:`KT1T2uYvmNmWv69ZmnVLM749LoYFt1AgqzF1`
